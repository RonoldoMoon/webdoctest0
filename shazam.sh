#!/bin/sh

echo -e "\nMake it so..\n"
cd /usr/share/nginx/html
mv index.html Oldindex.html

unzip -qq *.zip
rm        *.zip

FORMATICS=`ls | grep "Formatics"`
cd $FORMATICS

DITA=`ls | grep "dita"`
cd $DITA

mv  * ../../

cd ../../

rm -rf $FORMATICS $DITA

echo `ls`

echo -e "\nFile manipulation complete.\n"

cd /

echo -e "\nRunning web server\n"
nginx -g "daemon off;"
